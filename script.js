window.onload = () => {
	let formulaEl = document.querySelector("#formula");
	formulaEl.value = 0;
}

let permitNumbers = true;

function backspace() {
	let answerEl = document.querySelector("#answer");
	let formulaEl = document.querySelector("#formula");
	if (document.querySelector("#clear").classList.contains("del")) {
		if (formulaEl.value) {
			formulaEl.value = formulaEl.value.slice(0, -1);
			if (formulaEl.value=="") {
				formulaEl.value = 0;
				answerEl.innerHTML = 0;
				document.querySelector("#clear").innerHTML = "AC"
				document.querySelector("#clear").classList.remove("del")
			}
		}
	} else {
		formulaEl.value = 0;
		answerEl.innerHTML = 0;
	}
}

function update(_v) {
	permitNumbers = true;
	let answerEl = document.querySelector("#answer");
	let formulaEl = document.querySelector("#formula");
	let answer = _v;

	if (_v.length >= 11) {
		if (_v.indexOf(".") != -1) {
			answer = Number(Number(_v).toPrecision(10).substr(0, 12));
		} else {
			answer = Number(_v).toPrecision(10).substr(0, 12);
		}

		if (answerEl.innerText.indexOf("e") != -1) {
			answerEl.classList.add("smaller");
			formulaEl.value = "ERROR!!";
		}
	}

	if (answerEl.innerText.indexOf("e") != -1) {
		answerEl.classList.add("smaller");
		formulaEl.value = "ERROR!!";
	}

	// answer = Number(answer).toLocaleString();

	answerEl.innerHTML = answer;
	formulaEl.value = answer;

	document.querySelector("#clear").innerHTML = "AC"
	document.querySelector("#clear").classList.remove("del")

}

function append(_v) {
	let answerEl = document.querySelector("#answer");
	let formulaEl = document.querySelector("#formula");

	// formulaEl.value = formulaEl.value.replaceAll(",", "");

	if (formulaEl.value == 0) {
  	formulaEl.value = "";
  	formulaEl.value += _v;
	} else {
		if (permitNumbers) {
			formulaEl.value += _v;
		} else if (_v == "+" || _v == "-" || _v == "*" || _v == "/") {
			formulaEl.value += _v;
			permitNumbers = true;
		}
	}

	formulaEl.value = formulaEl.value.replace("++", "+");
	formulaEl.value = formulaEl.value.replace("+-", "-");
	formulaEl.value = formulaEl.value.replace("+*", "*");
	formulaEl.value = formulaEl.value.replace("+/", "/");
	formulaEl.value = formulaEl.value.replace("-+", "+");
	formulaEl.value = formulaEl.value.replace("--", "-");
	formulaEl.value = formulaEl.value.replace("-*", "*");
	formulaEl.value = formulaEl.value.replace("-/", "/");
	formulaEl.value = formulaEl.value.replace("/+", "+");
	formulaEl.value = formulaEl.value.replace("/-", "-");
	formulaEl.value = formulaEl.value.replace("/*", "*");
	formulaEl.value = formulaEl.value.replace("//", "/");
	formulaEl.value = formulaEl.value.replace("*+", "+");
	formulaEl.value = formulaEl.value.replace("*-", "-");
	formulaEl.value = formulaEl.value.replace("**", "*");
	formulaEl.value = formulaEl.value.replace("*/", "/");
	formulaEl.value = formulaEl.value.replace("+0", "+");
	formulaEl.value = formulaEl.value.replace("+00", "+");
	formulaEl.value = formulaEl.value.replace("-0", "-");
	formulaEl.value = formulaEl.value.replace("-00", "-");
	formulaEl.value = formulaEl.value.replace("/0", "/");
	formulaEl.value = formulaEl.value.replace("/00", "/");
	formulaEl.value = formulaEl.value.replace("*0", "*");
	formulaEl.value = formulaEl.value.replace("*00", "*");
	formulaEl.value = formulaEl.value.replace("..", ".");
	formulaEl.value = formulaEl.value.replace(/^[\+\-\*\/]/, "0");

	let left;
	let right;
	let leftOperand;
	let rightOperand;
	let operandArr = formulaEl.value.split(/[\+\-\*\/]/);
	let countDots = [];
	operandArr.forEach((numb, idx)=>{
		countDots[idx] = 0;
		for (let j = 0; j < operandArr.length; j++) {
			for (let i = 0; i < numb.length; i++) {
				if (numb[i] == "." && idx == j) {
					countDots[j] = countDots[j] + 1;
					if (countDots[j] >= 2) {
						formulaEl.value = formulaEl.value.slice(0, -1);
					}
				}
			}
		}
	});

	if (answerEl.innerHTML.length >= 11) {
		answerEl.innerHTML = Number(Number(answerEl.innerText).toPrecision(10));
	}

	if (formulaEl.value && _v != ".") {
		document.querySelector("#clear").innerHTML = "DEL"
		document.querySelector("#clear").classList.add("del")
		// formulaEl.value = formulaEl.value.slice(0, -1);
	}


	//ここから
	// let operatorArr = formulaEl.value.split(/[0-9]+/);
	// let commaseparatedArr = []
	// for (let i = 0; i < operatorArr.length; i++) {
	// 	operandArr[i] = Number(operandArr[i]).toLocaleString();
	// 	if (operandArr[i] != "" || operandArr[i] != "NaN") {
	// 		commaseparatedArr.push(operatorArr[i])
	// 		commaseparatedArr.push(operandArr[i])
	// 	}
	// }

	// commaseparatedArr.pop();
	// let equation = "";
	// for (let i = 0; i < commaseparatedArr.length; i++) {
	// 	equation = equation + commaseparatedArr[i]
	// }
	// formulaEl.value = equation;
	//ここまで

}

function calc() {
	let formulaEl;
	formulaEl = document.querySelector("#formula");
	// formulaEl.value = formulaEl.value.replaceAll(",", "");

	if (formulaEl.value.slice(-1) == "+" || formulaEl.value.slice(-1) == "-" ||
		formulaEl.value.slice(-1) == "*" || formulaEl.value.slice(-1) == "/") {
		formulaEl.value = formulaEl.value.slice(0, -1);
	}

	const v = formulaEl.value;
	const f = new Function('return ' + v);
	update(f().toString());
	permitNumbers = false;
}